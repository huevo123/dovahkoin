def read_wallet():
	with open("wallet.txt", 'r') as wallet_file:
		contents = wallet_file.read().split(',')
		one = int(contents[0].strip("["))
		five = int(contents[1])
		ten = int(contents[2].strip(']\n'))
		wallet = [one, five, ten]
	write_wallet(wallet)
	print(wallet)

def write_wallet(wallet):
	ones = input('ones?')
	fives = input('fives?')
	tens = input('tens?')
	wallet[0] += int(ones)
	wallet[1] += int(fives)
	wallet[2] += int(tens)
	with open("wallet.txt", 'w') as wallet_file:
	    wallet_file.write(str(wallet))

def reset_wallet():
    with open("wallet.txt", 'w') as wallet_file:
	    wallet_file.truncate()
	    wallet = [0, 0, 0]
	    wallet_file.write(str(wallet))
    return(wallet)

reset_wallet()