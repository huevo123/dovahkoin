import uuid, simplejson 

id_node = str(uuid.uuid4()).replace('-', '')

def new_transaction(amount = 5, type1 = 'caesium'):

    transaction = {
        "sender": id_node,
        "recipient": str(uuid.uuid4()).replace('-', ''),
        "amount": amount,
        "type": type1,
    }

    with open('transactions.json', 'r') as trans_file:
        myTrans = [line.strip() for line in trans_file]
        myTrans.append(transaction)
    with open('transactions.json', 'w') as trans_file:
        trans_file.truncate()
        simplejson.dump(myTrans, trans_file)

new_transaction()