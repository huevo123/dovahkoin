import hashlib, uuid, random
from time import sleep

while True:
    #how many digits we are trying to match
    difficulty = 2

    print('\nWelcome to the CHAZcoin network!\nHere we securely trade jazz\n ')

    hello = input("\nCommand 'mine' to begin\n")

    nonce = 0

    with open("wallet.txt", 'r+') as wallet_file:
        contents = wallet_file.read().split(',')
        # wallet
        jazz = int(contents[0].strip("["))
        gold = int(contents[1])
        caesium = int(contents[2].strip(']\n'))

    wallet = [jazz, gold, caesium]

    if(hello == "mine"):

        sleep(.8)
        print("\n\nBeginning...\n")
        sleep(1.6)
        new_caseuium = 0

        while(new_caseuium == 0):

            def print_wallet():
                jf = int(wallet[0])
                gf = int(wallet[1]) / 10
                cf = int(wallet[2]) / 1000

                print('{}\r'.format("\n      jazz = " + str(jf) + "      gold = " + str(gf) + "      caesium = " + str(cf)))


            difficulty_string = ('0'*difficulty)

            # c is random string to create a hash to solve
            c = uuid.uuid4()
            e = str(c).replace('-', '')

            # b is our hash, converted to hash so we can multiple
            b = int(hash((hashlib.sha256(e.encode()).hexdigest())))

            # we are looking for a nonce to make hash of x start with "0" up to difficulty
            x = (hashlib.sha256(str(b * nonce).encode())).hexdigest()    # while x up to difficulty =! the string we're looking for
            # add 1 to nonce until we find a nonce to satisfy our condition

            if(str(x)[:difficulty] != difficulty_string):
                nonce += 1
                x = (hashlib.sha256(str(b * nonce).encode())).hexdigest()

            elif(str(x)[:difficulty+2] == '0'*(difficulty+2)):
                    #caesium
                if(str(x)[:difficulty+3] == '0'*(difficulty+3)):
                    cae = int(random.random()*100)
                    wallet[2] += cae
                    with open("wallet.txt", 'r+') as wallet_file:
                        wallet_file.write(str(wallet))
                    print('\n                                            '+str(cae/1000)+' caesium added to wallet\n')
                    print('\a')
                    print_wallet()
                    new_caseuium += 1

                else:
                    #gold
                    ng = random.randrange(0,2)
                    wallet[1] += ng
                    with open("wallet.txt", 'r+') as wallet_file:
                        wallet_file.write(str(wallet))
                    print('\n               '+str(ng/10)+' gold added\n')

            else:
                #super jazz
                if(str(x)[:difficulty + 1] == '0' * (difficulty + 1)):
                    sj = random.randint(0, 200)
                    wallet[0] += sj
                    print('\n'+str(sj)+' jazz added\n')

                else:
                    #jazz
                    wallet[0] += random.randint(1, 5)


            with open("wallet.txt", 'r+') as wallet_file:
                wallet_file.write(str(wallet))

    if(hello == 'wallet'):
        jf = int(wallet[0])
        gf = int(wallet[1]) / 10
        cf = int(wallet[2]) / 1000
        print('{}\r'.format("\n      jazz = " + str(jf) + "      gold = " + str(gf) + "      caesium = " +str(cf)))

    if (hello == 'reset'):
        with open("wallet.txt", 'r+') as wallet_file:
            wallet_file.truncate()
            wallet = [0, 0, 0]
            wallet_file.write(str(wallet))
            print(wallet)